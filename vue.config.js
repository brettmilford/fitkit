module.exports = {
  //publicPath: process.env.NODE_ENV === 'production' &&
  //  ? '/' + process.env.CI_PROJECT_NAME + '/'
  //  : '/',
  //configureWebpack: config => {
  //  config.output.globalObject = "this"
  //},
  pwa: {
    appleMobileWebAppStatusBarStyle: "black-translucent",
    appleMobileWebAppCapable: 'yes',
    workboxPluginMode: 'GenerateSW',
    workboxOptions: {
      // override default which excludes icons
      exclude: [
        /\.map$/,
        /manifest\.json$/
      ]
    },
    themeColor: '#7957d5',
    iconPaths: {
      msTileImage: 'img/icons/mstile-150x150.png'
    },
    //manifestCrossorigin: 'use-credentials',
    manifestOptions: {
      "shortcuts": [
        {
          "name": "Start Workout",
          "url": "./workout"
        }
      ],
      "permissions": [
        "notifications",
        "unlimitedStorage"
      ]
    }
  }
}
