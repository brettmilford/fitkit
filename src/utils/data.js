import workout from '@/utils/workout.js'
import trainingTypes from '@/utils/trainingType.js'
import activity from '@/utils/activity.js'
import session from '@/utils/session.js'
import store from '@/store'
import notification from '@/utils/notification.js'
import { validate as uuidValidate } from 'uuid'

// a runtime join of workout/activity/type stores
export default {
    trainingType: trainingTypes.type,
    async getWorkout() {
        // NOTE: might require cache invalidation logic
        var workouts = await workout.getWorkout()

        for (let w of workouts) {
            w.type = this.trainingType[w.type]
            w.lastSession = await session.getLastSession(w.id)
            // NOTE: Should I evaluate type specific functions into data here?
            // i.e. calculate sets and overview table data etc
            // Reciever would have this information calculated for EVERY workout, in reality only 1 gets used at a time
            // the overview table data reasonably has the potential to be calculated for every workout, and potentially more than once
            // it might make sense to static that, but the sets data will only ever be calcualted once for 1 workout so would be a waste
            for (let s of w.exercises) {
                for (let e of s) {
                    // TODO: This is probably poor design of the getActivity() interface
                    // In that sending e.activity resulted in everything returned
                    // It was an un expected case
                    if (typeof e.activity !== "undefined")
                        e.activity = await activity.getActivity(e.activity)

                    if (w.lastSession && e.activity) {
                        const activity = w.lastSession.sets.filter(w => e.activity.id === w.activity.id)
                        if (activity.length > 0) {
                            e.reps =  Math.max(...activity.map(a => a.reps))
                            e.weight =  Math.max(...activity.map(a => a.weight))
                        }
                    }
                }
            }
        }
        return workouts
    },
    async jsonExport() {
        var data = {}

        await Promise.all(store.priStores.map(async s => {
            data[s] = await store.db.getAll(s)
        }))
        return "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(data));
    },
    async tsvExport(storeName) {
        const data = await store.db.getAll(storeName)
        var tables = []
        if (storeName === 'session') {
            const sets = data.flatMap(s => s.sets)
            const setsFields = Object.keys(sets[0])
            data.forEach(s => delete s.sets)
            tables.push(this.tsvString(sets, setsFields))
        }
        const fields = Object.keys(data[0])
        tables.push(this.tsvString(data, fields))
        return tables
    },
    tsvString(json, fields){
        var csv = json.map(function(r){
            return fields.map(function(f){
                return JSON.stringify(r[f], (key, value) => value === null ? '' : value)
            }).join('\t')
        })
        csv.unshift(fields.join('\t'))
        return "data:text/tsv;charset=utf8," + csv.join('\r\n')
    },
    async jsonImport(url) {
        fetch(url).then(response => {
            let data = response.json()
            store.priStores.map(primaryStore => {
                data[primaryStore].forEach(v => {
                    store.setStore(primaryStore, v, v.id)
                })
            })
        })
    },
    newWorkoutImport(w) {
       const activities = [... new Set(w.exercises.flat().map(a => a.activity).filter(el => el != null))]
       console.log(activities)
       Promise.all(activities.map((act) => {
           let a = { name: act }
           if (a.name == "Plank")
               a.durationActivity = true

           // only create a new activity it one by the same name doens't exist
           return activity.getActivityId(a.name).catch(() => {
               activity.newActivity(a)
           })
       })).then(async () => {
               w.type = trainingTypes.get(w.type)

               for (const set of w.exercises) {
                   for (const exercise of set) {
                       if ('activity' in exercise)
                           exercise.activity = await activity.getActivityId(exercise.activity)
                   }
               }

              workout.getWorkoutId(w.name).catch(() => {workout.newWorkout(w)})
       })
    },
    jsonFileImport(file) {
        const reader = new FileReader();
        reader.addEventListener('load', (event) => {
            try {
                let rawdata = event.target.result;
                let data = JSON.parse(rawdata)
                store.priStores.filter(s => s != 'activity').map(primaryStore => {
                   data[primaryStore]?.forEach(v => {

                       // convert dates to date objects
                       if ("startTime" in v)
                           v.startTime = new Date(v.startTime)

                       if ("finishTime" in v)
                           v.finishTime = new Date(v.finishTime)


                       if ("datetime" in v)
                           v.datetime = new Date(v.datetime)

                       // Check for possible duplicates
                       // Assume if they're named the same, they are the same, note not every store indexes on name
                       if (primaryStore == "workout") {
                           // if the workout store contains activity uuid's convert these to names
                           // newWorkoutImport can then do the de-dup
                           // activity store json must have been provided in the import to resolve the activity ids
                           for (const set of v.exercises) {
                               for (const exercise of set) {
                                   if (uuidValidate(exercise?.activity)) {
                                       let activityId = exercise.activity
                                       exercise.activity = data?.activity.find(a => a.id == activityId).name
                                   }
                               }
                           }
                           this.newWorkoutImport(v)
                       } else if (["food"].includes(primaryStore)) {
                           store.db.getFromIndex(primaryStore, 'name', IDBKeyRange.only(v.name)).catch(() => {
                               store.setStore(primaryStore, v)
                           })
                       } else {
                           store.db.add(primaryStore, v).catch((e) => {
                               if (e.name == "ConstraintError") {
                                   console.log(`Data: ${v.name} (${v.id}) already in store: ${primaryStore}`)
                                   throw(e)
                               } else {
                                   throw(e)
                               }
                           })
                       }
                   })
                })
                notification.dataImported()
            } catch (e) {
                console.error(e)
                notification.dataImportFailed(e)
            }
        });
        reader.readAsText(file)
    },
    jsonWorkoutImport(file) {
        const reader = new FileReader();
        reader.addEventListener('load', (event) => {
        try {
            let rawdata = event.target.result;
            let data = JSON.parse(rawdata)
            const activities = [... new Set(data.flatMap(item => item.exercises.flat())
                                                .map(a => a.activity).filter(el => el != null))]
            Promise.all(activities.map((act) => {
                let a = { name: act }
                if (a.name == "Plank")
                    a.durationActivity = true

                return activity.getActivityId(a.name).catch(() => {
                    activity.newActivity(a)
                })
            })).then(() => {
                data.forEach(async (w) => {
                    w.type = trainingTypes.get(w.type)

                    for (const set of w.exercises) {
                        for (const exercise of set) {
                            if ('activity' in exercise)
                                exercise.activity = await activity.getActivityId(exercise.activity)
                        }
                    }

                    workout.newWorkout(w)
                })
            })
            notification.workoutImported()
        } catch (e) {
            notification.workoutImportFailed(e)
        }
        })
        reader.readAsText(file)
    }
}
