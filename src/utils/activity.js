import store from '@/store'
import { v4 as uuidv4 } from 'uuid'

const OBJ_STORE = "activity"

export default {
    newActivity (activity) {
        store.setStore(OBJ_STORE, {
            id: uuidv4(),
            ...activity
        })
    },
    async getActivityId (activityName) {
        const range = IDBKeyRange.only(activityName)
        const val = await store.db.getAllFromIndex(OBJ_STORE,'name', range)
        return val[0]["id"]
    },
    async getActivity (activityId) {
        let val = null
        if (typeof activityId === "undefined") {
            val = await store.db.getAll(OBJ_STORE)
        } else {
            let range = IDBKeyRange.only(activityId)
            val = await store.db.get(OBJ_STORE, range)
        }
        return val
    },
    deleteActivity(id){
        store.db.delete(OBJ_STORE, id)
    }
}
