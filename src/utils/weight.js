import store from '@/store'
import { v4 as uuidv4 } from 'uuid'

const OBJ_STORE = "weight"

export default {
    newWeight (weight) {
        store.setStore(OBJ_STORE, {
            id: uuidv4(),
            ...weight
        })
    },
    async getWeight (weightId) {
        let val = null

        if (!store.db)
            await store.sync()

        if (typeof weightId === "undefined") {
            val = await store.db.getAll(OBJ_STORE)
        } else {
            let range = IDBKeyRange.only(weightId)
            val = await store.db.get(OBJ_STORE, range)
        }

        return val
    },
    async getWeightDate(fromDate = null, toDate = () => new Date()) {
        let keyrange = null
        if (fromDate <= toDate)
            keyrange = IDBKeyRange.bound(fromDate, toDate)
        else if (!fromDate)
            return keyrange = IDBKeyRange.upperBound(toDate)
        else
            return []

        if (!store.db)
            await store.sync()
        return store.db.getAllFromIndex(OBJ_STORE,'datetime', keyrange)
    },
    deleteWeight(id) {
        store.db.delete(OBJ_STORE, id)
    }
}
