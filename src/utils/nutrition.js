import store from '@/store'
import { v4 as uuidv4 } from 'uuid'

const OBJ_STORE = "food"

const UNIT = {
    "energy": [
        { "name": "Kilojoule", "unit": "kJ", power: 3, base: 'joule'},
        { "name": "Kilocalorie", "unit": "kcal", power: 3, base: 'calorie'},
        { "name": "Joule", "unit": "J", power: 0, base: 'joule' },
        { "name": "Megajoule", "unit": "MJ", power: 6, base: 'joule' },
    ],
    "generic": [
        {"name": "Serve", "unit": "serve"},
    ],
    "mass": [
        {"name": "Gram", "unit": "g", power: 0, base: 'metric'},
        {"name": "Milligram", "unit": "mg", power: -3, base: 'metric'},
        {"name": "Kilogram", "unit": "kg", power: 3, base: 'metric'}
    ],
    "volume": [
        {"name": "Litre", "unit": "L", power: 0, base: 'metric'},
        {"name": "Millilitre", "unit": "ml", power: -3, base: 'metric'}
    ],
    "length": [
        {"name": "Centimetre", "unit": "cm", power: -2, base: 'metric'}
    ]
}

export default {
    get units() {
        let units = UNIT
        for (const p in units) {
            units[p].forEach(u => {
                u.type = p
            })
        }
        return units
    },
    newFood (food) {
        store.setStore(OBJ_STORE, {
            id: uuidv4(),
            ...food
        })
    },
    async getFood (foodId) {
        let val = null
        if (typeof foodId === "undefined") {
            val = await store.db.getAll(OBJ_STORE)
        } else {
            let range = IDBKeyRange.only(foodId)
            val = await store.db.get(OBJ_STORE, range)
        }
        return val
    },
    recordNutrition(nutrition){
        store.setStore('nutrition', {
            id: uuidv4(),
            ...nutrition
        })
    },
    convertMass(qty, unita, unitb) {
        qty = qty * 10 ** (unita.power - unitb.power)
        if (unita.base != unitb.base){
            // NOTE: not confident this works in all matchings/cases for 'imperial'
            if (unita.base == 'imperial')
                qty *= 0.453
            else if (unita.base == 'metric')
                qty /= 0.453
        }
        return qty
    },
    convertEnergy(qty, unita, unitb) {
        qty = qty * 10 ** (unita.power - unitb.power)
        if (unita.base != unitb.base) {
            if (unitb.base == "calorie")
                qty /= 4.18
            else if (unitb.base == "joule")
                qty *= 4.18
        }
        return qty
    },
    async getNutritionDate(fromDate = null, toDate = () => new Date()) {
        let keyrange = null
        if (fromDate <= toDate)
            keyrange = IDBKeyRange.bound(fromDate, toDate)
        else if (!fromDate)
            return keyrange = IDBKeyRange.upperBound(toDate)
        else
            return []

        if (!store.db)
            await store.sync()
        return store.db.getAllFromIndex('nutrition','datetime', keyrange)
    },
    deleteNutrition(id){
        store.db.delete('nutrition', id)
    },
    deleteFood(id){
        store.db.delete('food', id)
    }
}
