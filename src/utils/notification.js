import {
    ToastProgrammatic as BuefyT,
    DialogProgrammatic as BuefyD
} from 'buefy'

export default {
    state: {
        audioInit: false,
        sound: null
    },

    initAudio() {
      console.log("Init Audio Event Triggered")
        this.state.sound = new window.Audio(require('@/assets/notification.mp3'))
        this.state.sound.volume = 0
        this.state.sound.muted = true
        this.state.sound.play()
    },
    playAudio() {
      console.log("Play audio event")
        this.state.sound.volume = 1
        this.state.sound.muted = false
        this.state.sound.play()
    },
    nativeAlert() {
           new Notification('Rest timer Up!', {vibrate: [200, 100, 200]})
           //window.navigator.vibrate(200)
    },
    nextInterval() {
        if (("Notification" in window) && !(Notification.permission === 'denied' || Notification.permission === 'default')) {
           new Notification('Next interval!', {vibrate: [200, 100, 200]})
        } else {
            BuefyT.open({
                message: 'Next interval!',
            })
        // see Remove audio notifications bug
        //this.playAudio()
        }
    },
    restComplete(msg) {
        if (("Notification" in window) && !(Notification.permission === 'denied' || Notification.permission === 'default')) {
           new Notification('Rest timer Up!', {vibrate: [200, 100, 200]})
        } else {
            BuefyT.open({
                message: msg,
                duration: 3000,
            })
        // see Remove audio notifications bug
        //this.playAudio()
        }
    },
    stopWorkout(onConfirm) {
        BuefyD.confirm({
            title: 'Are you sure?',
            message: 'Finish this workout early?',
            type: 'is-danger',
            hasIcon: true,
            icon: 'times-circle',
            iconPack: 'fa',
            ariaRole: 'alertdialog',
            ariaModal: true,
            onConfirm: onConfirm,
        })
    },
    cancelWorkout(onConfirm) {
        BuefyD.confirm({
            title: 'Are you sure?',
            message: "Cancel this workout?<br/>(It won't be recorded)",
            type: 'is-danger',
            hasIcon: true,
            icon: 'times-circle',
            iconPack: 'fa',
            ariaRole: 'alertdialog',
            ariaModal: true,
            onConfirm: onConfirm,
        })
    },
    finishWorkout(onConfirm) {
        BuefyD.confirm({
            title: 'Are you sure?',
            message: 'Finish and record this workout?',
            hasIcon: true,
            ariaRole: 'alertdialog',
            ariaModal: true,
            onConfirm: onConfirm,
        })
    },
    nutritionSaved(foodName){
        BuefyT.open({
            message: foodName + " saved.",
            duration: 3000,
        })
    },
    weightSaved(){
        BuefyT.open({
            message: "Weight saved.",
            duration: 3000,
        })
    },
    workoutSaved(){
        BuefyT.open({
            message: "Quick Workout saved.",
            duration: 3000
        })
    },
    nutritionCopied(){
        BuefyT.open({
            message: "Nutrition records copied.",
            duration: 3000,
        })
    },
    workoutImported() {
         BuefyT.open({
            message: "Workout imported.",
            duration: 3000,
        })
    },
    workoutImportFailed(e) {
         BuefyT.open({
            message: `Workout import failed: ${e}`,
            duration: 3000,
            type: 'is-danger'
        })
    },
    workoutAdded(name) {
         BuefyT.open({
            message: `Workout ${name} added.`,
            duration: 3000,
         })
    },
    activityAdded(name) {
         BuefyT.open({
            message: `Activity ${name} added.`,
            duration: 3000,
         })
    },
    dataImported() {
         BuefyT.open({
            message: "Data imported.",
            duration: 3000,
        })
    },
    dataImportFailed(e) {
         BuefyT.open({
            message: `Data import failed: ${e}`,
            duration: 3000,
            type: 'is-danger'
        })
    },
}
