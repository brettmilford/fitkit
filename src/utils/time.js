export default {
    padNumber: function (n, width, z) {
        /* This function pads number 'n', to a width of 'width' with characters 'z'
         * If no character is supplied (z) the number will be padded with 0 */

        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    },
    formatTime: function(data) {
        /* This function takes time values in the format of
         * [{unit: unit, text: string, numeric: number},...]
         * And returns strings in the format of HH:mm:ss omitting HH if 0 */
        let str = ""

        if (data[0].numeric > 0) {
            str += "" + data[0].text + ":"
        }

        str += "" + data[1].text + ":"
        str += "" + data[2].text
        return str
    },
    timeData: function(ms) {
        /* This function takes a numeric value of milliseconds and converts it to
         * [{unit: unit, text: string, numeric: number},...] data for time representation */
        let ss = ms/1000
        let hrs = ~~(ss / 3600)

        let mins = ~~((ss % 3600) / 60)

        let secs = ~~ss % 60

        return [
          {
            "unit": "hours",
            "text": this.padNumber(hrs, 2),
            "numeric": hrs
          },
          {
            "unit": "minutes",
            "text": this.padNumber(mins,2),
            "numeric": mins
          },
          {
            "unit": "seconds",
            "text": this.padNumber(secs,2),
            "numeric": secs
          }]
    },
    timeDelta: function(ms) {
        /* Takes a numeric value of milliseconds and returns a formatted string */
        let time = this.timeData(ms)
        return this.formatTime(time)
    }
}
