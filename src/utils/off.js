export default {
    async search(query){
        const queryEncoded = encodeURIComponent(query)
        const query_url = "https://au.openfoodfacts.org/cgi/search.pl?search_simple=1&action=process&json=1&search_terms=" + queryEncoded
        const response = await fetch(query_url,{
            headers: {
                'User-Agent': "FitKit - https://fitkit.asdfg.io/"
            }
        })
        return response.json()
    },
    async getProduct(code){
        const query_url = "https://au.openfoodfacts.org/api/v0/product/" + code + ".json"
        const response = await fetch(query_url, {
            headers: {
                'User-Agent': "FitKit - https://fitkit.asdfg.io/"
            }
        })
        return response.json()
    }
}
