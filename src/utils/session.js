import store from '@/store'
import { v4 as uuidv4 } from 'uuid'

const OBJ_STORE = 'session'

export default {
    state: {
        sync: true,
    },
    startSession: function(selection) {
        //NOTE: could store the intermediate session at this point
        return {
            'id': uuidv4(),
            'name': selection.name,
            'type': selection.type.id,
            'category': selection.type.category,
            'workoutId': selection.id,
            'startTime': new Date(),
            'finishTime': 'incomplete',
        }
    },
    finishSession: function(session, data) {
        session.finishTime = new Date()
        // bit of a hacky
        if (typeof data === "number") {
            session.rounds = data - 1
        } else {
            session.sets = data
        }
        this.logWorkoutSession(session)
        return session
    },
    logGenericSession(data) {
        this.logWorkoutSession({
            'id': uuidv4(),
            ...data
        })
    },
    logWorkoutSession(data) {
      /*
       * sets = 2
       * session = 1
       * sync: sets + session //(3) when both pending
       * sync: sets           //(2) when only sets pending
       * sync: session        //(1) when only session pending
       * syncSets: [].length of session.sets
       * Elements are 1 or 0 pending sync status of the corresponding set
       */
        console.log('logging workout session for ' + data.name)
        //NOTE: there is a bug here, setting the primary store will fail
        //whilst setting the shaddow store will succeed with an undefined key
        store.setStore(OBJ_STORE, data)
        if (this.state.sync) {
            console.log('creating shadow entry')
            if (data.sets)
                store.setStore((OBJ_STORE + "_shadow"), { store: OBJ_STORE, key: data.id, sync: 3, syncSets: new Array(data.sets.length).fill(1)})
            else
                store.setStore((OBJ_STORE + "_shadow"), { store: OBJ_STORE, key: data.id, sync: 1})
        }
    },
    async getSession(sessionId) {
        let val = null
        if (typeof workoutId === "undefined") {
            val = await store.db.getAll(OBJ_STORE)
        } else {
            let range = IDBKeyRange.only(sessionId)
            val = await store.db.get(OBJ_STORE,range)
        }
        return val
    },
    async getLastSession(workoutId) {
        let range = IDBKeyRange.only(workoutId)
        let val = await store.db.getAllFromIndex(OBJ_STORE,'workoutId',range)
        if (val.length > 0) {
            val.sort((a,b) => { return a.finishTime - b.finishTime })
            return val.pop()
        }
        console.log('No previous session exists')
        return null

    },
    async getSessionDate(fromDate = null, toDate = () => new Date()) {
        let keyrange = null
        //console.log("fromDate: " + fromDate + " toDate: " + toDate + " check:" + (fromDate <= toDate))

        if (fromDate <= toDate)
            keyrange = IDBKeyRange.bound(fromDate, toDate)
        else if (!fromDate)
            return keyrange = IDBKeyRange.upperBound(toDate)
        else
            return []

        if (!store.db)
            await store.sync()

        return store.db.getAllFromIndex(OBJ_STORE,'date', keyrange)

    },
    async workoutsSync(backend) {
        console.log('syncing data...')
        // get data from shadow store -> store
        const range =  IDBKeyRange.lowerBound(1)
        // potentially need to cap each run of this
        const toSync = await store.db.getAllFromIndex((OBJ_STORE + '_shadow'), 'sync', range)
        console.log(toSync)

        await Promise.all(toSync.map(async (ses) => {
            console.log(ses)
            const d = await store.db.get(OBJ_STORE, ses.key)
            console.log(d)

            //sync sets
            if (ses.sync > 1) {
                d.sets.forEach((s,i) => {
                    console.log(s)
                    if (ses.syncSets[i] > 0) {
                        try {
                            ses.syncSets[i] = 0
                            backend.put(s)
                        } catch (_) {
                            ses.syncSets[i] = 1
                        }
                    }
                })

                delete d.sets
            }

            //sync session
            if (ses.sync == 1 || ses.sync == 3) {
                try {
                    ses.sync -= 1
                    backend.put(d)
                } catch (_) {
                    ses.sync += 1
                }
            }

            // if all sets are synced update 'sync'
            if (typeof ses.syncSets !== 'undefined' && ses.syncSets.every((v) => v == 0))
                ses.sync -= 2

            //put update to shadow store
            await store.db.put((OBJ_STORE + '_shadow'), ses)
        }))
    },
    deleteSession(id) {
        store.db.delete(OBJ_STORE, id)
    }
}
