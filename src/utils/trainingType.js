//import activity from '@/utils/activity.js'

const DEFAULT_INT = 30
const TRAINING_TYPE =
    [{
        "name": "Calisthenics Training",
        "category": "resistance",
        "categoryName": "Resistance Training",
        "componentRoute": "/workout/resistance",
        "previewColumns": [
            { field: 'activityName', label: 'Activity' },
            { field: 'interval', label: 'Interval' },
            { field: 'sets', label: 'Sets' },
            { field: 'reps', label: 'Reps' },
            { field: 'area', label: 'Area' },
            { field: 'weight', label: 'Weight' },
            { field: 'volume', label: 'Volume' },
            { field: 'super', label: 'Super'}
        ],
        "summaryColumns": [
            { field: 'activityName', label: 'Activity' },
            { field: 'sets', label: 'Sets'},
            { field: 'mostReps', label: 'Best #Reps' },
            { field: 'mostWeight', label: 'Best Weight' },
            { field: 'volume', label: 'Volume' }
        ],
        "columns": [
            { field: 'activityName', label: 'Activity' },
            { field: 'set', label: 'Set' },
            { field: 'reps', label: 'Reps' },
            { field: 'interval', label: 'Interval' },
            { field: 'super', label: 'Super'}
        ],
        "previewRows": resistanceTrainingData,
        "summaryRows": resistanceTrainingSummaryData,
        "sets": resistanceTrainingSets
    },
    {
        "name": "Interval Training",
        "categoryName": "Cardiovascular Training",
        "category": "cardio",
        "componentRoute": "/workout/interval",
        "previewColumns": [
            { field: 'ppo', label: "PPO" },
            { field: 'interval', label: 'Interval' },
        ], "summaryColumns": [
            { field: 'level', label: 'Level' },
            { field: 'interval', label: 'Interval'},
            { field: 'times', label: 'Times' }
        ],
        "previewRows": cardioTrainingData,
        "summaryRows": cardioTrainingSummaryData,
    },
    {
        "name": "Functional Strength Training",
        "category": "resistance",
        "categoryName": "Resistance Training",
        "componentRoute": "/workout/resistance",
        "previewColumns": [
            { field: 'activityName', label: 'Activity' },
            { field: 'interval', label: 'Interval' },
            { field: 'sets', label: 'Sets' },
            { field: 'reps', label: 'Reps' },
            { field: 'area', label: 'Area' },
            { field: 'weight', label: 'Weight' },
            { field: 'volume', label: 'Volume' },
            { field: 'super', label: 'Super'}
        ],
        "summaryColumns": [
            { field: 'activityName', label: 'Activity' },
            { field: 'sets', label: 'Sets'},
            { field: 'mostReps', label: 'Best #Reps' },
            { field: 'mostWeight', label: 'Best Weight' },
            { field: 'volume', label: 'Volume' },
        ],
        "columns": [
            { field: 'activityName', label: 'Activity' },
            { field: 'set', label: 'Set' },
            { field: 'reps', label: 'Reps' },
            { field: 'weight', label: 'Weight' },
            { field: 'interval', label: 'Interval' },
            { field: 'super', label: 'Super'}
        ],
        "previewRows": resistanceTrainingData,
        "summaryRows": resistanceTrainingSummaryData,
        "sets": resistanceTrainingSets
    },
    {
        "name": "Steady State Training",
        "category": "cardio",
        "categoryName": "Cardiovascular Training",
        "componentRoute": "/workout/cardio",
        "previewColumns": [
            { field: 'ppo', label: "PPO" }
        ],
        "previewRows": cardioTrainingData,
    },
    {
        "name": "Generic",
        "categories": [
            {category: 'resistance', categoryName: 'Resistance Training'},
            {category: 'cardio', categoryName: 'Cardiovascular Training'}
        ],
    }]

function resistanceTrainingData(data) {
    // used for formatting a table for a workout overview
    var flattened = []
    let setof = data.exercises.length
    data.exercises.map((ex,i) => {
      let setin = i + 1
      ex.map((e, j) => {
          e.super = setin + "/" + setof
          if (typeof e.interval === 'undefined')
              e.interval = DEFAULT_INT
          e.activityName = e.activity["name"]
          e.area = e.sets * e.reps
          e.volume = e.sets * e.reps * (e.weight ??= 0)
          e.i = i,
          e.j = j,
          flattened.push(e)
       })
    })
    return flattened
}

function cardioTrainingData(data) {
    return data.exercises[0]
}

function resistanceTrainingSets(data) {
    /*
     * returns a list of set objects
     * objects are order sensitive
     * [{
     * 'activity': {},
     * 'activityName': blah,
     * 'set': 1/n,
     * 'reps': int,
     * 'interval': int,
     * 'super': 1,n
     * },...]
     *
     */
    let selection = data.selection
    let superof = selection.exercises.length
    return selection.exercises.flatMap((ex, j) => {
        let sets = []
        if (data.addWarmup) {
            sets = ex.flatMap((e) => {
                const workingWeight = e.weight
                e.super = "Warm up"
                if (workingWeight > 0) {
                    // warmup by weight
                    const increment = workingWeight > 50 ? workingWeight > 100 ? 40 : 20 : 10
                    const minWeight = e.activity.name === 'Deadlift' ? 40 : 20
                    const numSets = ~~((workingWeight - minWeight) / increment)
                    let weight = minWeight
                    let sets = []
                    for (let i = 1; i <= numSets; i++) {
                        sets.push({
                            'activity': e.activity,
                            'activityName': e.activity.name,
                            'setin': i,
                            'set': i + '/' + numSets,
                            'reps': e.reps,
                            'weight': weight,
                            'interval': DEFAULT_INT, // warmups always short rest period
                            'super': e.super,
                            'isWorkingSet': false,
                            'isDurationSet': e.activity.durationActivity,
                        })
                        weight = weight + increment
                    }
                    return sets
                } else {
                    // warmup by reps
                    const increment = ~~(e.reps / (e.sets - 1))
                    const numSets = ~~(e.reps / increment)
                    let sets = []
                    let reps = increment
                    for (let i = 1; i <= numSets; i++) {
                        sets.push({
                            'activity': e.activity,
                            'activityName': e.activity.name,
                            'setin': i,
                            'set': i + '/' + numSets,
                            'reps': reps,
                            'weight': e.weight,
                            'interval': DEFAULT_INT, // warmups always short rest period
                            'super': e.super,
                            'isWorkingSet': false,
                            'isDurationSet': e.activity.durationActivity,
                        })
                        reps = reps + increment
                    }
                    return sets
                }
            })
        }

        // iterate by set first then exercise
        // this way the sets are interleaved
        let numSets = ex[0].sets
        let superin = j + 1
        for (let i = 1; i <= numSets; i++) {

            sets = sets.concat(ex.flatMap((e) => {
                e.super = superin + "/" + superof
                if (typeof e.interval === 'undefined')
                    e.interval = DEFAULT_INT

                return{
                    'activity': e.activity,
                    'activityName': e.activity.name,
                    'setin': i,
                    'set': i + '/' + e.sets,
                    'reps': e.reps,
                    'weight': e.weight,
                    'interval': e.interval,
                    'superin': superin,
                    'superof': superof,
                    'super': e.super,
                    'isWorkingSet': true,
                    'isDurationSet': e.activity.durationActivity,
                }
            }))
        }
        return sets
    })
}

function resistanceTrainingSummaryData (data) {
    const activities = new Set(data.map(s => s.activity))
    var rowData = []
    activities.forEach(a => {
        const activity = data.filter(s => s.activity === a)
        const mostReps = Math.max(...activity.map(s => s.reps))
        const mostWeight = Math.max(...activity.map(s => s.weight))
        const volume = activity.map(s => s.reps * s.weight).reduce((a,c) => { return a + c })
        rowData.push({
            activity: a,
            activityName: a.name,
            sets: activity.length,
            mostReps: mostReps,
            mostWeight: mostWeight,
            volume: volume,
        })
    })
    return rowData
}

function cardioTrainingSummaryData (data) {
    return [data]
}

export default {
    get (v) {
        if (typeof v === "object") {
            return TRAINING_TYPE.findIndex(type => type.name === v.name)
        } else if (typeof v === "string") {
            return TRAINING_TYPE.findIndex(type => type.name === v)
        } else if (typeof v === "number") {
            return TRAINING_TYPE[v].name
        }
    },
    get type() {
        var trainingType = []
        TRAINING_TYPE.map(t => {
            t.id = TRAINING_TYPE.findIndex(type => type.name === t.name)
            trainingType.push(t)
        })
        return Object.seal(trainingType)
    },
}
