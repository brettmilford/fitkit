import store from '@/store'
import { v4 as uuidv4 } from 'uuid'

const OBJ_STORE = "workout"

export default {
    newWorkout (workout) {
        // should check 'types' are passed as uuids or convert, same with 'activity'
        //workout.id = uuidv4()
        store.setStore('workout', {
            id: uuidv4(),
            ...workout
        })
    },
    async getWorkout (workoutId) {
        let val = null
        if (typeof workoutId === "undefined") {
            val = await store.db.getAll(OBJ_STORE)
        } else {
            let range = IDBKeyRange.only(workoutId)
            val = await store.db.get(OBJ_STORE, range)
        }
        return val
    },
    deleteWorkout(workoutId){
        store.db.delete(OBJ_STORE, workoutId)
    },
    async getWorkoutId(workoutName) {
        const val = await store.db.getAllFromIndex(OBJ_STORE, 'name', IDBKeyRange.only(workoutName))
        return val[0]["id"]
    }
}
