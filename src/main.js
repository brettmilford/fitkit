import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import './registerServiceWorker.js'
Vue.config.productionTip = false

// this will import the entire fa solid library
//import { fas } from '@fortawesome/free-solid-svg-icons'
import { faDumbbell,
         faRunning,
         faHistory,
         faCog,
         faPlay,
         faPlus,
         faRedo,
         faMinus,
         faTimesCircle,
         faCheckCircle,
         faSync,
         faEdit,
         faDownload,
         faCloudUploadAlt,
         faWeight,
         faClock,
         faCalendarDay,
         faAngleLeft,
         faExclamationCircle,
         faCarrot,
         faBarcode,
         faChevronDown,
         faChevronUp,
         faChevronLeft,
         faChevronRight,
         faArrowUp,
         faUpload,
         faFilter,
         faAngleRight,
         faTrash} from '@fortawesome/free-solid-svg-icons'

import { library } from '@fortawesome/fontawesome-svg-core'
library.add(faDumbbell,
            faRunning,
            faHistory,
            faCog,
            faPlay,
            faPlus,
            faRedo,
            faMinus,
            faAngleLeft,
            faAngleRight,
            faTimesCircle,
            faSync,
            faEdit,
            faDownload,
            faWeight,
            faClock,
            faCalendarDay,
            faCloudUploadAlt,
            faExclamationCircle,
            faCarrot,
            faBarcode,
            faChevronDown,
            faChevronUp,
            faChevronLeft,
            faChevronRight,
            faArrowUp,
            faUpload,
            faFilter,
            faTrash,
            faCheckCircle)

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.component('vue-fontawesome', FontAwesomeIcon)

//import Buefy from 'buefy'
//import 'buefy/dist/buefy.css'
//Vue.use(Buefy, {
//  defaultIconComponent: 'vue-fontawesome',
//  defaultIconPack: 'fas'
//})

import { ConfigProgrammatic,
         Button,
         Icon,
         Input,
         Navbar,
         Notification,
         Toast,
         Numberinput,
         Autocomplete,
         Slider,
         Table,
         Tabs,
         Field,
         Select,
         Switch,
         Datetimepicker,
         Datepicker,
         Snackbar,
         Modal,
         Collapse,
         Upload,
         Radio,
         Progress,
         Checkbox,
         Dropdown,
         Dialog } from 'buefy'

import 'buefy/dist/buefy.css'

Vue.use(Button)
Vue.use(Input)
Vue.use(Navbar)
Vue.use(Notification)
Vue.use(Toast)
Vue.use(Numberinput)
Vue.use(Slider)
Vue.use(Table)
Vue.use(Tabs)
Vue.use(Field)
Vue.use(Select)
Vue.use(Icon)
Vue.use(Switch)
Vue.use(Modal)
Vue.use(Snackbar)
Vue.use(Datetimepicker)
Vue.use(Datepicker)
Vue.use(Autocomplete)
Vue.use(Collapse)
Vue.use(Upload)
Vue.use(Radio)
Vue.use(Progress)
Vue.use(Dialog)
Vue.use(Checkbox)
Vue.use(Dropdown)
ConfigProgrammatic.setOptions({
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas'
})

new Vue({
  router,
  mounted () {
    store.sync()
  },
  render: h => h(App)
}).$mount('#app')
