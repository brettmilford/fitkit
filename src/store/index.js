import { openDB } from 'idb'
import trainingType from '@/utils/trainingType.js'

const shadowStores = new RegExp("shadow$")

const DB_NAME = "FitKit"
const SCHEMA_VERSION = 4 //NOTE: change when OBJ_STORES changes
const OBJ_STORES = [
    {
        name: 'session',
        options: {
            keyPath: 'id'
        },
        index : [
            {
              key: 'date',
              val: 'finishTime',
              opt: {
                  local: 'auto',
                  unique: false
              }
            },
            {
              key: 'name',
              val: 'name',
            },
            {
                key: 'workoutId',
                val: 'workoutId',
            }
        ],
        versionIntroduced: 4 //NOTE: change this when introduced, or index is modified
    },
    {
        name: 'session_shadow',
        options: {
            //NOTE: This forces 'inline' keys, i.e. to update an entry the keypath must be in the value
            keyPath:'id',
            autoIncrement: true
        },
        index: [
            {
              key: 'sync',
              val: 'sync',
            },
            {
                key: 'key',
                val: 'key'
            }
        ],
        versionIntroduced: 1
    },
    {
        // workout < type, exercise < activity
        // requires type, exercises, number of rounds (default 1)
        name: 'workout',
        options: {
            keyPath: 'id'
        },
        index: [
            {
              key: 'name',
              val: 'name',
            },
            {
              key: 'type',
              val: 'type',
            }
        ],
        versionIntroduced: 1
    },
    {
        // activity > exercise
        // simplest form its a name + a uuid
        // can store ref to 'best' set
        name: 'activity',
        options: {
            keyPath: 'id'
        },
        index: [
            {
              key: 'name',
              val: 'name',
            }
        ],
        versionIntroduced: 1
    },
    {
        name: 'weight',
        options: {
            keyPath: 'id'
        },
        index: [
            {
              key: 'datetime',
              val: 'datetime',
            }
        ],
        versionIntroduced: 1
    },
    {
        name: 'food',
        options: {
            keyPath: 'id'
        },
        index: [
            {
              key: 'name',
              val: 'name',
            }
        ],
        versionIntroduced: 2
    },
    {
        name: 'nutrition',
        options: {
            keyPath: 'id',
        },
        index: [
            {
              key: 'datetime',
              val: 'datetime',
            }
        ],
        versionIntroduced: 2
    },
    {
        name: 'setting',
        options: {
            autoIncrement: true
        },
        index: [
            {
                key: 'key',
                val: 'key'
            },
            {
                key: 'value',
                val: 'value'
            }
        ],
        versionIntroduced: 3
    }
]

export default {
    stores: OBJ_STORES.map(s => s.name),
    priStores: OBJ_STORES.map(s => s.name).filter(s => {return !shadowStores.test(s)}),
    db: null,
    async sync () {
        this.db = await openDB(DB_NAME, SCHEMA_VERSION, {
            upgrade(db, oldVersion, newVersion, transaction) {
                // for some reason 'this' isn't mapped here
                console.log('onupgradeneeded: ' + oldVersion + " => " + newVersion)

                async function recreateStore (s) {
                    console.log('recreating store ' + s.name)

                    let objectStore = transaction.objectStore(s.name)
                    const currentStoreData = await objectStore.getAll()
                    db.deleteObjectStore(s.name)

                    createStore(s)

                    //db update code for schema change
                    if (SCHEMA_VERSION == 4 && s.name == "session") {
                        let tt = trainingType.type
                        transaction.oncomplete = () => {
                            currentStoreData.map(ent => {
                                ent.category = tt[ent.type].category
                                db.put(s.name, ent)
                            })
                        }

                    } else {
                        transaction.oncomplete = () => {
                            currentStoreData.map(ent => {
                                db.put(s.name, ent)
                            })
                        }
                    }

                }

                function createStore (s) {

                    if ('options' in s)
                        var store = db.createObjectStore(s.name, s.options)
                    else
                        store = db.createObjectStore(s.name)

                    if ('index' in s)
                        s.index.forEach((i) => {
                            store.createIndex(i.key, i.val, i.opt)
                        })
                    return store
                }

                for (oldVersion; oldVersion <= newVersion; ++oldVersion) {
                    console.log('upgrading to schema version ' + oldVersion)
                    OBJ_STORES
                        .filter(s => { return s.versionIntroduced == oldVersion})
                        .map(s => {
                            if (Object.values(db.objectStoreNames).includes(s.name)){
                                recreateStore(s)
                            } else {
                                console.log('creating store ' + s.name)
                                createStore(s)
                            }
                        })
                }
            },
            blocked() {
                console.log(DB_NAME + " version: " + SCHEMA_VERSION + " blocked.")
            },
            blocking() {
                console.log(DB_NAME + " version: " + SCHEMA_VERSION + " blocking.")
            },
            terminated() {
                console.log(DB_NAME + " version: " + SCHEMA_VERSION + " terminated.")
            }
        })
    },
    async setStore(objstore, value, key) {
        console.log('setting store: ' + objstore);

        if (typeof key == 'undefined')
            await this.db.add(objstore, value)
        else
            await this.db.put(objstore, value, key)
    },
}
