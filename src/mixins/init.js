import activity from '@/utils/activity.js'
import trainingType from '@/utils/trainingType.js'
import workout from '@/utils/workout.js'

export default {
    data() {
        return {
            //NOTE: mixins DO NOT share data() across components
            initialised: window.localStorage.getItem('init')
        }
    },
    methods: {
        initApp() {
            if (!this.initialised) {
                this.askPermission()
                this.initData()

                let initialised = Date.now()
                window.localStorage.setItem('init', initialised)
                return initialised
            }
        },
        askPermission() {
            if (!("Notification" in window)) {
                console.log("This browser does not support notifications.");
            } else {
                if(checkNotificationPromise()) {
                    Notification.requestPermission()
                                .then((permission) => {
                                    handlePermission(permission);
                                })
                } else {
                    Notification.requestPermission((permission) => {
                        handlePermission(permission);
                    })
                }
            }

            function checkNotificationPromise () {
                try {
                    Notification.requestPermission().then()
                } catch(e) {
                    return false
                }

                return true
            }

            function handlePermission (permission) {
                // Whatever the user answers, we make sure Chrome stores the information
                if (!('permission' in Notification)) {
                    Notification.permission = permission
                }
            }
        },
        initData() {
            // data init function to convert json schema to indexeddb
            // 'data' referring to the json shouldn't be referrenced outside of this func
            // everything in main section should refer to the db
            // This should exercise most set and get functions in main
            const data = require('@/assets/data.json')

            const activities = [... new Set(data.workout.flatMap(item => item.exercises.flat())
                                                .map(a => a.activity).filter(el => el != null))]
            activities.forEach(act => {
                let a = { name: act }
                if (a.name == "Plank")
                    a.durationActivity = true
                activity.newActivity(a)
            })

            data.workout.forEach(async (w) => {
                w.type = trainingType.get(w.type)

                for (const set of w.exercises) {
                    for (const exercise of set) {
                        if (exercise.activity != null)
                            exercise.activity = await activity.getActivityId(exercise.activity)
                    }
                }

                workout.newWorkout(w)
            })
        }
    }
}
