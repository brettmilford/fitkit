import store from '@/store'
import nutrition from '@/utils/nutrition.js'
const OBJ_STORE = 'setting'

export default {
  data() {
    return {
             setting: {
                 weekCommencesDay: 'Monday',
                 sessionsPerWeek: null,
                 cardioMinutesPerWeek: null,
                 defaultEnergyUnit: nutrition.units.energy.filter(u => { return u.name == 'Kilojoule'})[0],
                 height: null,
                 targetWeight: null,
                 initialWeight: null,
                 weightUnit: nutrition.units.mass.filter(u => { return u.name == 'Kilogram' })[0],
                 energyBudgetType: null,
                 energyBudgetValue: null,
                 energyBudgetScheduleValue: {
                    "Sunday": null,
                    "Monday": null,
                    "Tuesday": null,
                    "Wednesday": null,
                    "Thursday": null,
                    "Friday": null,
                    "Saturday": null
                 },
                 proteinTarget: null,
                 fibreTarget: null,
                 percentageBodyFat: null,
                 initialPercentageBodyFat: null,
                 addWarmup: false,
             }
    }
  },
  async mounted () {
      await this.settingSync()
      // Set defaults
      new Array('weekCommencesDay','defaultEnergyUnit','weightUnit', 'energyBudgetScheduleValue','addWarmup').map(s => {
          store.db.getFromIndex(OBJ_STORE, 'key', IDBKeyRange.only(s)).then((result) => {
              if (!result) {
                  store.db.getKeyFromIndex(OBJ_STORE, "key", IDBKeyRange.only(s)).then((result) =>{
                      store.setStore(OBJ_STORE, {
                          "key": s,
                          "value": this.setting[s]
                      }, result)
                  })
              }
          })
      })
  },
    methods: {
        async settingSync() {
            if (!store.db)
                await store.sync()
            return store.db.getAll(OBJ_STORE).then(val => {
                val.map(v => {
                    this.setting[v.key] = v.value
             })
         })
        }
    }
}
