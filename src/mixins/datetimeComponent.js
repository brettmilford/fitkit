Date.prototype.getWeek = function() {
  var date = new Date(this.getTime());
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
                        - 3 + (week1.getDay() + 6) % 7) / 7);
}

Date.prototype.getWeekYear = function() {
  var date = new Date(this.getTime());
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  return date.getFullYear();
}

const SHORT_MONTH = [
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "Jun",
                    "Jul",
                    "Aug",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dec"
                ]

const LONG_MONTH = [
                "January",
                "Feburary",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ]

const DAY = [
                "Sunday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday"
            ]

function WeekCommencingDate(date, dayCommence = 1) {
  let wc = new Date(date.getFullYear(), date.getMonth(), date.getDate(),0,0,0,0)
  return new Date(wc.setDate(wc.getDate() - (wc.getDay() + (7 - dayCommence)) % 7))
}


function WeekEndingDate(date, dayCommence = 1) {
  let we = new Date(date.getFullYear(), date.getMonth(), date.getDate(),23,59,59,999)
  return new Date(we.setDate(we.getDate() + ((6 + dayCommence) - we.getDay()) % 7))
}

function MonthCommencingDate(date) {
    return new Date(date.getFullYear(), date.getMonth(), 1,0,0,0,0)
}

function MonthEndingDate(date) {
    return new Date(date.getFullYear(), date.getMonth() + 1, 0,23,59,59,999)
}

function ithString(date) {
   if (Math.floor(date / 10) == 1)
     return date + "th"

  switch (date % 10) {
      case 1:
          return date + "st"
      case 2:
          return date + "nd"
      case 3:
          return date + "rd"
      default:
          return date + "th"
  }
}

function isLeapYear(year) {
  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)
}

function dateString(date, type='full') {
    if (type == "full")
        return ithString(date.getDate()) + " of " + LONG_MONTH[date.getMonth()] + ", " + date.getFullYear()
    else if (type == "month")
        return LONG_MONTH[date.getMonth()] + ", " + date.getFullYear()

}

export let datetimeParent = {
    // NOTE: supply viewRoute data
    props: {
        'year': {
            default: () => new Date().getFullYear()
        },
        'month': {
            type: String,
            default: () => {
                return SHORT_MONTH[new Date().getMonth()]
            }
        },
        'week': {
            default: () => new Date().getWeek()
        },
        'date': {
            default: () => new Date().getDate()
        },
    },
    data () {
        return {
            today: new Date(),
            dateTo: null, //NOTE: null here would override component prop's default
            dateFrom: null,
            navBy: "default",
        }
    },
    beforeRouteUpdate (to, from, next) {
        console.log(to)
        if (to.params.date != null) {
        // is a date route
            console.log("date route")
            if (to.params.date < 1){
                console.log("rolling back")
                let params = {}
                if (SHORT_MONTH.indexOf(to.params.month) < 1) {
                console.log("year")
                    params.year = parseInt(to.params.year) - 1
                    params.month = SHORT_MONTH[SHORT_MONTH.length - 1]
                } else {
                    console.log("month")
                    params.year = to.params.year
                    params.month = SHORT_MONTH[SHORT_MONTH.indexOf(to.params.month) - 1]
                }
                if (params.month == 'Feb') {
                    if (isLeapYear(params.year)){
                    console.log("leap feb")
                        params.date = 29
                    } else {
                    console.log("feb")
                        params.date = 28
                    }
                } else if (["Sep","Apr","Jun","Nov"].includes(params.month)) {
                    console.log("30 day")
                    params.date = 30
                } else {
                    console.log("31 day")
                    params.date = 31
                }
                next({name: `${this.viewRoute}Day`, params: params})
            } else if (to.params.date > 28 && to.params.month == "Feb" && !isLeapYear(to.params.year)){
                console.log("rolling forward feb")
                let params = {}
                params.month = "Mar"
                params.date = 1
                params.year = to.params.year
                next({ name: `${this.viewRoute}Day`, params: params })
            } else if (to.params.date > 29 && to.params.month == "Feb" && isLeapYear(to.params.year)) {
                console.log("rolling forward leap feb")
                let params = {}
                params.month = "Mar"
                params.date = 1
                params.year = to.params.year
                next({ name: `${this.viewRoute}Day`, params: params })
            } else if  (to.params.date > 30 && ["Sep","Apr","Jun","Nov"].includes(to.params.month)) {
                console.log("rolling forward 30 day")
                let params = {}
                params.year = to.params.year
                params.month = SHORT_MONTH[SHORT_MONTH.indexOf(to.params.month) + 1]
                params.date = 1
                next({name: `${this.viewRoute}Day`, params: params})
            } else if  (to.params.date > 31) {
                console.log("rolling forward")
                let params = {}
                if (to.params.month == "Dec") {
                    console.log("year")
                    params.year = parseInt(to.params.year) + 1
                    params.month = SHORT_MONTH[0]
                } else {
                    console.log("month")
                    params.year = to.params.year
                    params.month = SHORT_MONTH[SHORT_MONTH.indexOf(to.params.month) + 1]
                }
                params.date = 1
                next({name: `${this.viewRoute}Day`, params: params})
            } else {
               next()
            }
        } else if (to.params.week != null) {
            // is a week route
            this.navBy = "week"
            if (to.params.week == this.today.getWeek()) {
                this.dateFrom = null
                this.dateTo = new Date()
                this.exerciseRoute = { name: "Exercise" }
                this.nutritionRoute = { name: "Nutrition" }
                this.weightRoute = { name: "Weight" }
                next({ name: "Dashboard" })
            } else if (to.params.week < 1) {
                next({name: `${this.viewRoute}Week`, params: { year: parseInt(to.params.year) -1, week: 52 }})
            } else if (to.params.week > 52) {
                next({name: `${this.viewRoute}Week`, params: { year: parseInt(to.params.year) +1, week: 1 }})
            }
        } else if (to.params.month != null) {
            // is a month route
            console.log("is a month route")
        } else if (to.params?.year) {
            console.log("is a year route")
        } else {
          console.log("is a catchall route")
          this.exerciseRoute = { name: "Exercise" }
          this.nutritionRoute = { name: "Nutrition" }
          this.weightRoute = { name: "Weight" }
          next()
        }
    },
    created () {
        if (this.$route.params.date != null) {
            this.navBy = "day"
            this.dateTo = new Date(this.$route.params.year, SHORT_MONTH.indexOf(this.$route.params.month), this.$route.params.date, 23,59,59,999)
            this.dateFrom = this.wc
            let params = {
                    year: this.dateTo.getFullYear(),
                    month: SHORT_MONTH[this.dateTo.getMonth()],
                    date: this.dateTo.getDate(),
                }
            this.exerciseRoute = {
                name: "ExerciseDay",
                params: params
            }
            this.nutritionRoute = {
                name: "NutritionDay",
                params: params
            }
            this.weightRoute = {
                name: "WeightDay",
                params: params
            }
        } else if (this.$route.params.week != null) {
           this.navBy = "week"
           this.dateTo = this.weekDateTo
           this.dateFrom = this.weekDateFrom
            let params = {
               year: this.dateTo.getFullYear(),
               week: this.dateTo.getWeek()
            }
           this.exerciseRoute = {
               name: "ExerciseWeek",
               params: params
           }
           this.nutritionRoute = {
               name: "NutritionWeek",
               params: params
           }
           this.weightRoute = {
               name: "WeightWeek",
               params: params
           }
        } else {
            this.dateTo = new Date()
        }
    },
    watch: {
        $route(to) {
            console.log(to)
            if (to.params.date != null) {
                this.navBy = "day"
                this.dateTo = new Date(this.$route.params.year, SHORT_MONTH.indexOf(this.$route.params.month), this.$route.params.date, 23,59,59,999)
                this.dateFrom = this.wc
                let params = {
                   year: this.dateTo.getFullYear(),
                   month: SHORT_MONTH[this.dateTo.getMonth()],
                   date: this.dateTo.getDate(),
                }
                this.exerciseRoute = {
                    name: "ExerciseDay",
                    params: params
                }
                this.nutritionRoute = {
                    name: "NutritionDay",
                    params: params
                }
                this.weightRoute = {
                    name: "WeightDay",
                    params: params
                }
            } else if (to.params.week != null) {
               this.navBy = "week"
               this.dateTo = this.weekDateTo
               this.dateFrom = this.weekDateFrom
                let params = {
                   year: this.dateTo.getFullYear(),
                   week: this.dateTo.getWeek()
                }
               this.exerciseRoute = {
                   name: "ExerciseWeek",
                   params: params
               }
               this.nutritionRoute = {
                   name: "NutritionWeek",
                   params: params
               }
               this.weightRoute = {
                   name: "WeightWeek",
                   params: params
               }
            } else {
                this.dateTo = new Date()
            }
        }
    },
    methods: {

        getDateOfISOWeek(w, y) {
            var simple = new Date(y, 0, 1 + (w - 1) * 7);
            var dow = simple.getDay();
            var ISOweekStart = simple;
            if (dow <= 4)
                ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
            else
                ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
            return ISOweekStart;
       }
    },
    computed: {
        weekDateFrom(){
            let monday = this.getDateOfISOWeek(this.week, this.year)
            return monday
        },
        weekDateTo(){
            let monday = this.getDateOfISOWeek(this.week, this.year)
            return new Date(monday.getFullYear(), monday.getMonth(), monday.getDate() + 6, 23, 59, 59, 999)
        },
        todayString() {
            if (this.navBy == "week" && this.dateTo.getWeek() != this.today.getWeek())
                return DAY[this.wc.getDay()]
            else if (this.navBy == "day")
               return DAY[this.dateTo.getDay()]
            else
                return DAY[this.today.getDay()]
        },
        dateString(){
            if (this.navBy == "week" && this.dateTo.getWeek() != this.today.getWeek())
               return "Week of the " + ithString(this.wc.getDate()) + " of " + LONG_MONTH[this.wc.getMonth()] + ", " + this.wc.getFullYear()
            else if (this.navBy == "day")
               return ithString(this.dateTo.getDate()) + " of " + LONG_MONTH[this.dateTo.getMonth()] + ", " + this.dateTo.getFullYear()
            else
                return ithString(this.today.getDate()) + " of " + LONG_MONTH[this.today.getMonth()] + ", " + this.today.getFullYear()
        },
        wc () {
            return WeekCommencingDate(this.dateTo)
        },
        we () {
            return WeekEndingDate(this.dateTo)
        },
        shortMonthString () {
            return SHORT_MONTH
        }
    }
}

export let datetimeChild = {
    // NOTE: BYO sync ()
    props: {
        'dateTo': {
            type: Date,
            default: () => new Date()
        },
        'dateFrom': {
            type: Date,
            default: null
        }
    },
    data () {
        return {
            to: this.dateTo,
            from: this.dateFrom
        }
    },
    mounted () {
        if (!this.from) this.from = this.wc
        this.sync()
    },
    watch: {
        dateTo(val){
            this.to = val
            this.sync()
        },
        dateFrom(val){
            if (val)
               this.from = val
            else
                this.from = this.wc
            this.sync()
        }
    },
    methods: {
        dateString: dateString
    },
    computed: {
        wc () {
            return WeekCommencingDate(this.dateTo)
        },
        weeks(){
            let one_week = 1000 * 60 * 60 * 24 * 7
            return ~~((this.to - this.from) / one_week) + 1
        },
        timeSpan(){
            return this.weeks >= 2 ? this.weeks >= 4 ? "month": "fortnight" : "week"
        },
        lastEntryDateString: function () {
            if (this.stats.lastEntry?.finishTime)
                return ithString(this.stats.lastEntry.finishTime.getDate()) + " of " + LONG_MONTH[this.stats.lastEntry.finishTime.getMonth()] + ", " + this.stats.lastEntry.finishTime.getFullYear()
            else if (this.stats.lastEntry?.datetime)
                return ithString(this.stats.lastEntry.datetime.getDate()) + " of " + LONG_MONTH[this.stats.lastEntry.datetime.getMonth()] + ", " + this.stats.lastEntry.datetime.getFullYear()
        },
        daysComplete() {
            let one_day = 1000 * 3600 * 24
            return ~~((this.to - this.from) / one_day ) + 1
        },
        daysOfTheWeek() {
            return DAY
        },
        dateToString() {
            return dateString(this.to)
        },
        wcString() {
            return dateString(this.wc)
        },
        we () {
            return WeekEndingDate(this.dateTo)
        },
        dateFromString() {
            return dateString(this.from)
        },
        mc () {
            return MonthCommencingDate(this.dateTo)
        },
        me () {
            return MonthEndingDate(this.dateTo)
        },
        mcString () {
            return dateString(this.mc, "month")
        }
   }
}
