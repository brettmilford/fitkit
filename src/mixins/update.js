export default {
  data() {
    return {
      registration: null,
      refreshing: false,
      updateExists: false
    }
  },
  created() {
    document.addEventListener('swUpdated',
      (event) => {
        this.registration = event.detail
        this.updateExists = true
      }, { once: true })

    navigator.serviceWorker.addEventListener('controllerchange',
      () => {
        if (!this.refreshing){
          this.refreshing = true
          //NOTE: This ensures the root of the app is reloaded from the server, avoiding server side 404
          window.location.assign(document.defaultView.location.origin)
        }
      })
  },
  methods: {
    refreshApp() {
      this.updateExists = false
      if (this?.registration && this.registration?.waiting)
        this.registration.waiting.postMessage({ type: 'SKIP_WAITING' })
    },
  },
}
