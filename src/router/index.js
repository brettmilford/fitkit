import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/exercise',
    name: "Exercise",
    component: () => import(/* webpackChunkName: "exercise" */ '../components/Exercise/View.vue'),
  },
  {
    path: '/exercise/:year(\\d{4})',
    name: 'ExerciseYear',
    props: true,
    component: () => import(/* webpackChunkName: "exercise" */ '../components/Exercise/View.vue'),
  },
  {
    path: '/exercise/:year(\\d{4})/:month([a-zA-z]{3})',
    name: 'ExerciseMonth',
    props: true,
    component: () => import(/* webpackChunkName: "exercise" */ '../components/Exercise/View.vue'),
  },
  {
    path: '/exercise/:year(\\d{4})/:week(\\d+)',
    name: 'ExerciseWeek',
    props: true,
    component: () => import(/* webpackChunkName: "exercise" */ '../components/Exercise/View.vue'),
  },
  {
    path: '/exercise/:year(\\d{4})/:month([a-zA-z]{3})/:date(\\d+)',
    name: "ExerciseDay",
    props: true,
    component: () => import(/* webpackChunkName: "exercise" */ '../components/Exercise/View.vue')
  },
  {
    path: '/workout',
    component: () => import(/* webpackChunkName: "workout" */ '../components/Workout/Controller.vue'),
    children: [
      {
        name: 'Workout',
        path: '',
        component: () => import(/* webpackChunkName: "workout" */ '../components/Workout/Select.vue'),
      },
      {
        name: "Interval",
        path: 'interval',
        component: () => import(/* webpackChunkName: "workout-int" */ '../components/Workout/TrainingType/Interval.vue'),
        beforeEnter: (to, from, next) => {
          if (from.path !== '/workout')
            next('/workout')
          else
            next()
        }
      },
      {
        name: "Cardio",
        path: 'cardio',
        component: () => import(/* webpackChunkName: "workout-sst" */ '../components/Workout/TrainingType/Cardio.vue'),
        beforeEnter: (to, from, next) => {
          if (from.path !== '/workout')
            next('/workout')
          else
            next()
        }
      },
      {
        name: 'Resistance',
        path: 'resistance',
        component: () => import(/* webpackChunkName: "workout-run" */ '../components/Workout/TrainingType/Resistance.vue'),
        // eslint-disable-next-line no-unused-vars
        beforeEnter: (to, from, next) => {
          if (from.path !== '/workout')
            next('/workout')
          else
            next()
        }
      },
      {
        name: "Summary",
        path: 'summary',
        component: () => import(/* webpackChunkName: "workout-sum" */ '../components/Workout/Summary.vue'),
        beforeEnter: (to, from, next) => {
          if (from.fullPath === '/workout/resistance' || from.fullPath === '/workout/interval' || from.fullPath === '/workout/cardio')
            next()
          else
            next('/workout')
        }
      },
    ],
  },
  {
    path: '/weight',
    name: "Weight",
    component: () => import(/* webpackChunkName: "weight" */ '../components/Weight/View.vue')
  },
  {
    path: '/weight/:year(\\d{4})/:week(\\d+)',
    name: "WeightWeek",
    props: true,
    component: () => import(/* webpackChunkName: "weight" */ '../components/Weight/View.vue')
  },
  {
    path: '/weight/:year(\\d{4})/:month([a-zA-z]{3})/:date(\\d+)',
    name: "WeightDay",
    props: true,
    component: () => import(/* webpackChunkName: "weight" */ '../components/Weight/View.vue')
  },
  {
    path: '/weight/record',
    component: () => import(/* webpackChunkName: "weight" */ '../components/Weight/Input.vue')
  },
  {
    path: '/nutrition',
    name: "Nutrition",
    component: () => import(/* webpackChunkName: "nutrition" */ '../components/Nutrition/View.vue')
  },
  {
    path: '/nutrition/:year(\\d{4})/:week(\\d+)',
    name: "NutritionWeek",
    props: true,
    component: () => import(/* webpackChunkName: "nutrition" */ '../components/Nutrition/View.vue')
  },
  {
    path: '/nutrition/:year(\\d{4})/:month([a-zA-z]{3})/:date(\\d+)',
    name: "NutritionDay",
    props: true,
    component: () => import(/* webpackChunkName: "nutrition" */ '../components/Nutrition/View.vue')
  },
  {
    path: '/nutrition/record',
    component: () => import(/* webpackChunkName: "nutrition" */ '../components/Nutrition/Input.vue')
  },
  {
    path: '/settings',
    component: () => import(/* webpackChunkName: "settings" */ '../components/Settings.vue'),
  },
  {
    path: '/:year(\\d{4})',
    name: 'Year',
    component: () => import(/* webpackChunkName: "dashboard" */ '../components/Dashboard/View.vue'),
    props: true,
  },
  {
    path: '/:year(\\d{4})/:month([a-zA-z]{3})',
    name: 'Month',
    component: () => import(/* webpackChunkName: "dashboard" */ '../components/Dashboard/View.vue'),
    props: true,
  },
  {
    path: '/:year(\\d{4})/:week(\\d+)',
    name: 'Week',
    component: () => import(/* webpackChunkName: "dashboard" */ '../components/Dashboard/View.vue'),
    props: true,
  },
  {
    path: '/:year(\\d{4})/:month([a-zA-z]{3})/:date(\\d+)',
    name: 'Day',
    component: () => import(/* webpackChunkName: "dashboard" */ '../components/Dashboard/View.vue'),
    props: true,
  },
  {
    path: '/',
    name: "Dashboard",
    component: () => import(/*webpackChunkName: "dashboard" */ "../components/Dashboard/View.vue")
  },
  //NOTE: root dynamic route always last
  {
    name: '404',
    path: '*',
    redirect: { path: '/' }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
