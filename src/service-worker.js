workbox.setConfig({
  debug: true,
});

workbox.precaching.precacheAndRoute([]);

// cache images, staleWhileRevalidate method
// good for js / css which aren't precached
workbox.routing.registerRoute(
  /\.(?:png|gif|jpg|jpeg|svg|wav|mp3|css|js)$/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'images',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 60,
        maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
      }),
    ],
  }),
);

// cache workouts_sets content w/ networkFist method
workbox.routing.registerRoute(
  new RegExp('https://(.*).gitlab.io/(.*)'),
  new workbox.strategies.NetworkFirst({
    cacheName: 'api',
  }),
);

workbox.routing.registerRoute(
  new RegExp('https://(.*).asdfg.io/(.*)'),
  new workbox.strategies.NetworkFirst({
    cacheName: 'api',
  }),
);

workbox.routing.registerRoute(
  new RegExp('https://fitkit.asdfg.io'),
  new workbox.strategies.NetworkFirst({
    cacheName: 'start_url',
  }),
);

// cache fonts from google with cacheFirst method
workbox.routing.registerRoute(
  new RegExp('https://fonts.(?:googleapis|gstatic).com/(.*)'),
  new workbox.strategies.CacheFirst({
    cacheName: 'googleapis',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 30,
      }),
    ],
  }),
);

// might need to consider versioning service workers down the track
