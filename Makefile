##
# Project Title
#
# @file
# @version 0.1

# microk8s registry
# Make sure insecure registries is configured in /etc/docker/daemon.json
# {
#   "insecure-registries" : ["localhost:32000"]
# }

image = fitkit/app
registry = localhost:32000
version := $(shell git describe --tags --always --abbrev=8 --dirty)

build:
	sed -e 's|:VERSION_STR|:$(version)|' src/components/About.vue
	docker build -t $(image):$(version) .

push:
	docker tag $(image):$(version) $(registry)/$(image):$(version)
	docker push $(registry)/$(image):$(version)

deployment:
	sed -e 's|:latest|:$(version)|g' deployment.yaml | kubectl apply -f -

deploy: build push deployment

dev:
	npm run serve

dev-ngrok:
	ngrok http 8080

serve: build
	/bin/sh -c trap 'docker kill `docker ps -n1 -q`' INT; \
	           docker run -p 8080:80 --rm $(image):$(version)

killall:
	- killall ngrok
	- docker kill $(shell docker ps -n1 -q)

clean:
	- git clean -f -x

.PHONY: clean dev-ngrok dev deploy deployment push build serve killall

# end
